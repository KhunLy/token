﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Token.api.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
        public string Password { get; set; }
        public string Pseudo { get; set; }
    }
}