﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Token.api.Models
{
    public class LoginModel
    {
        public string Pseudo { get; set; }
        public string Password { get; set; }
    }
}