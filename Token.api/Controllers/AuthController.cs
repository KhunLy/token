﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Token.api.Models;
using ToolBox.JWT;

namespace Token.api.Controllers
{
    public class AuthController : ApiController
    {
        private static List<UserModel> _fake_db
            = new List<UserModel>
            {
                new UserModel { Id = 1, Pseudo = "Martine", Password = "test1234=", RoleName = "ADMIN" },
                new UserModel { Id = 2, Pseudo = "Simon", Password = "simon", RoleName = "CUSTOMER" },
            };
        private TokenService _tokenService;

        public AuthController(TokenService tokenService)
        {
            _tokenService = tokenService;
        }

        [HttpPost]
        [Route("api/login")]
        public string Login(LoginModel model)
        {
            // Check in db
            UserModel c = _fake_db.FirstOrDefault(
                u => u.Pseudo.ToLower() == model.Pseudo.ToLower()
                && u.Password == model.Password
            );
            // Ca marche
            if(c != null)
            {
                // on envoie le token
                c.Password = null;
                return _tokenService.Encode(c);
            }
            // Ca marche pas
            // on l'envoit c...
            throw new HttpResponseException(HttpStatusCode.Unauthorized);
                
        }
    }
}
